using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] float health = 100;

    float shotCounter;
    [SerializeField] float minTimeBetweenShots = 0.2f;
    [SerializeField] float maxTimeBetweenShots = 3f;
    [SerializeField] float projectileSpeed = 6f;
    [SerializeField] GameObject baseProjectileSprite;


    float halfHeight;

    // Start is called before the first frame update
    void Start()
    {
        var spriteObject = GetComponent<SpriteRenderer>();
        halfHeight = spriteObject.bounds.size.y / 2f;
        shotCounter = Random.Range(minTimeBetweenShots, maxTimeBetweenShots);
        StartCoroutine(ShootCoroutine());
    }

    IEnumerator ShootCoroutine()
    {
        while(true)
        {
            yield return new WaitForSeconds(Random.Range(minTimeBetweenShots, maxTimeBetweenShots));
            Shoot();
        } 

    }

    void Shoot()
    {
        GameObject projectile = Instantiate(baseProjectileSprite, new Vector2(transform.position.x, transform.position.y - halfHeight*2), new Quaternion(0, 0, 180, 0)) as GameObject;
        projectile.GetComponent<Rigidbody2D>().velocity = new Vector2(0, -projectileSpeed);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        DamageDealer damageDealer = other.GetComponent<DamageDealer>();
        if (damageDealer)
            ProcessHit(damageDealer);
    }

    private void ProcessHit(DamageDealer damageDealer)
    {
        health -= damageDealer.GetDamage();
        damageDealer.Hit();
        if (health <= 0)
            Destroy(gameObject);
    }
}
