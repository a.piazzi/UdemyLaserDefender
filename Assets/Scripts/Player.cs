using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    // config
    [Header("Player")]
    [SerializeField] int health = 200;
    [SerializeField] float moveSpeed = 10f;
    [SerializeField] float topPadding = 0f;
    [SerializeField] float bottomPadding = 0f;
    [SerializeField] float leftPadding = 0f;
    [SerializeField] float rightPadding = 0f;

    [Header("Projectile")]
    [SerializeField] GameObject baseProjectileSprite;
    [SerializeField] float projectileSpeed = 10f;
    [SerializeField] float projectileFiringPeriod = 0.2f;

    // boundaries
    float xMin;
    float xMax;
    float yMin;
    float yMax;

    //
    Coroutine firingCoroutine;
    float timeLastShot;
    float width;
    float height;
    float halfWidth;
    float halfHeight;


    // Start is called before the first frame update
    void Start()
    {
        var spriteObject = GetComponent<SpriteRenderer>();
        width = spriteObject.bounds.size.x;
        height = spriteObject.bounds.size.y;
        halfWidth = width / 2f;
        halfHeight = height / 2f;
        SetUpMoveBoundaries();
        timeLastShot = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        Fire();
    }

    private void SetUpMoveBoundaries()
    {
        Camera gameCamera = Camera.main;
        var minPoint = gameCamera.ViewportToWorldPoint(new Vector3(0 + leftPadding, 0 + bottomPadding, 0));
        var maxPoint = gameCamera.ViewportToWorldPoint(new Vector3(1 - rightPadding, 1 - topPadding, 0));

        xMin = minPoint.x + halfWidth;
        yMin = minPoint.y + halfHeight;
        xMax = maxPoint.x - halfWidth;
        yMax = maxPoint.y - halfHeight;
    }

    private void Move()
    {
        var deltaX = Input.GetAxis("Horizontal") * Time.deltaTime * moveSpeed;
        var deltaY = Input.GetAxis("Vertical") * Time.deltaTime * moveSpeed;

        var newXPos = Mathf.Clamp(transform.position.x + deltaX, xMin, xMax); // check left and right boundary
        var newYPos = Mathf.Clamp(transform.position.y + deltaY, yMin, yMax);
        transform.position = new Vector2(newXPos, newYPos);
    }

    private void Fire()
    {
        if(Input.GetButtonDown("Fire1"))
        {
            if(firingCoroutine == null)
                firingCoroutine = StartCoroutine(FireContinously());
        }
        if(Input.GetButtonUp("Fire1"))
        {
            if (firingCoroutine != null)
            { 
                StopCoroutine(firingCoroutine);
                firingCoroutine = null;
            }
        }
    }

    IEnumerator FireContinously()
    {
        while (true)
        {
            if (Time.time - timeLastShot >= projectileFiringPeriod)
            {
                GameObject laser = Instantiate(baseProjectileSprite, new Vector3(transform.position.x, transform.position.y + halfHeight), Quaternion.identity) as GameObject;
                laser.GetComponent<Rigidbody2D>().velocity = new Vector2(0, projectileSpeed);
                timeLastShot = Time.time;
            }

            yield return new WaitForSeconds(projectileFiringPeriod);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        DamageDealer damageDealer = other.GetComponent<DamageDealer>();
        if (damageDealer)
            ProcessHit(damageDealer);
    }

    private void ProcessHit(DamageDealer damageDealer)
    {
        health -= damageDealer.GetDamage();
        damageDealer.Hit();
        if (health <= 0)
            Destroy(gameObject);
    }
}
