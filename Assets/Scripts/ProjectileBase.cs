using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileBase : MonoBehaviour
{
    float halfHeight;

    private void Start()
    {
        halfHeight = GetComponent<SpriteRenderer>().bounds.size.x / 2;
    }

    private void Update()
    {
        var viewPos = Camera.main.WorldToViewportPoint(new Vector3(transform.position.x, transform.position.y - halfHeight));
        if(viewPos.y > 1)
            Destroy(gameObject);
    }
}
